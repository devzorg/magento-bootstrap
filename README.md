Magento Bootstrap
=================

This is a theme based on the [Twitter Bootstrap](http://twitter.github.com/bootstrap) framework for the [Magento Commerce](http://www.magentocommerce.com) system.

**PLEASE NOTE THIS IS A WORK IN PROGRESS**

Installation
------------

1. Copy the files to the Magento installation folder.

2. Use the theme in the Magento backend.

Customization
-------------

This theme is meant to be developed further, so you would probably copy this project and work from there.

Credits
-------

[Twitter Bootstrap](http://twitter.github.com/bootstrap) was designed and built with all the love in the world [@twitter](http://twitter.com/twitter) by [@mdo](http://twitter.com/mdo) and [@fat](http://twitter.com/fat).

Magento adaption by Oleg Batischev